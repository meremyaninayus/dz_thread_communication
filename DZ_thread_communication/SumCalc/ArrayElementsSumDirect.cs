﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_thread_communication.SumCalc
{
    public class ArrayElementsSumDirect : ISumArrayElements
    {
        public long SumArrayElements(int[] array)
        {
            long sum = 0;
            foreach (int x in array)
            {
                sum = sum + x;
            }
            return sum;
        }
    }
}
