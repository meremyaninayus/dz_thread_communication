﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DZ_thread_communication.SumCalc
{
    public class ArrayElementsSumThread : ISumArrayElements
    {
        public long SumArrayElements(int[] array)
        {
            var preData = new List<ThreadData>();
            var threadList = new List<Thread>();
            for (int i = 0; i < 10; i++)
            {
                var threadData = new ThreadData() { Sum = 0 };
                preData.Add(threadData);
                var sourceIndex = array.Length / 10 * i;
                var destLength = array.Length / 10;
                threadData.Array = new int[array.Length / 10];
                Array.Copy(array, sourceIndex, threadData.Array, 0, destLength);
                Thread t = new Thread(SumArrayElementsInThread);
                t.Start(threadData);
                threadList.Add(t);
            }
            foreach (var thread in threadList)
            {
                thread.Join();
            }
            long sum = 0;
            foreach (var threadData in preData)
            {
                sum += threadData.Sum;
            }
            return sum;
        }

        private void SumArrayElementsInThread(object threadDataObj)
        {
            var threadData = (ThreadData)threadDataObj;
            long sum = 0;
            foreach (int x in threadData.Array)
            {
                sum += x;
            }
            threadData.Sum = sum;
        }

        private class ThreadData
        {
            public int[] Array { get; set; }
            public long Sum { get; set; }
        }
    }
}
