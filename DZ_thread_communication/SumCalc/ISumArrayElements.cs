﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_thread_communication.SumCalc
{
    public interface ISumArrayElements
    {
        public long SumArrayElements(int[] array);
    }
}
