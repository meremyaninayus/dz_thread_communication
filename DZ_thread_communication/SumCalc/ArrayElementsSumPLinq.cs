﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DZ_thread_communication.SumCalc
{
    public class ArrayElementsSumPLinq : ISumArrayElements
    {
        public long SumArrayElements(int[] array)
        {
            return array.Select(x => (long)x).AsParallel().Sum();
        }
    }
}
