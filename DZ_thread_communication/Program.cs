﻿using DZ_thread_communication.SumCalc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DZ_thread_communication
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var array = new int[3];
            array[0] = 10000;
            array[1] = 1000_000;
            array[2] = 10_000_000;
            for (int i = 0; i< 3; i++ )
            {
                Console.WriteLine($"Обыное вычисление для {array[i]} элементов: {TimeCounter.MeasureTime(array[i], new ArrayElementsSumDirect())}");
                Console.WriteLine($"Вычисление в 10-ти потоках для {array[i]} элементов: {TimeCounter.MeasureTime(array[i], new ArrayElementsSumThread())}");
                Console.WriteLine($"Вычисление с помощью PLinq для {array[i]} элементов: {TimeCounter.MeasureTime(array[i], new ArrayElementsSumPLinq())}");
            }
            Console.ReadKey();
        }

    }
}
