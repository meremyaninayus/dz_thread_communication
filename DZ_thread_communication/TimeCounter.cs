﻿using DZ_thread_communication.SumCalc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DZ_thread_communication
{
    public static class TimeCounter
    {

        public static long MeasureTime(int arrayLength, ISumArrayElements sumArrayElements)
        {
            var array = new int[arrayLength];
            for (int i = 1; i < arrayLength+1; i++)
            {
                array[i - 1] = i;
            }

            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine("SumArrayElements: " + sumArrayElements.SumArrayElements(array));
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
    }
}
